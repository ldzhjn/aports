# Contributor: Leo <thinkabit.ukim@gmail.com>
# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Maintainer: team/gnome <ablocorrea@hotmail.com>
pkgname=evince
pkgver=46.1
pkgrel=0
pkgdesc="Simple document viewer for GTK+"
url="https://wiki.gnome.org/Apps/Evince"
# s390x blocked by mozjs91 -> nautilus-dev
arch="all !s390x"
license="GPL-2.0-or-later"
depends="
	adwaita-icon-theme
	gsettings-desktop-schemas
	"
depends_dev="
	gobject-introspection-dev
	gtk+3.0-dev
	libspectre-dev
	libxml2-dev
	poppler-dev
	tiff-dev
	"
makedepends="
	$depends_dev
	adwaita-icon-theme-dev
	cairo-dev
	dbus-dev
	desktop-file-utils
	djvulibre-dev
	gdk-pixbuf-dev
	glib-dev
	gnome-desktop-dev
	gspell-dev
	gst-plugins-base-dev
	gstreamer-dev
	itstool
	libarchive-dev
	libgxps-dev
	libhandy1-dev
	libsecret-dev
	meson
	"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang $pkgname-libs"
source="https://download.gnome.org/sources/evince/${pkgver%.*}/evince-$pkgver.tar.xz"

# secfixes:
#   3.32.0-r1:
#     - CVE-2019-11459
#   3.24.0-r2:
#     - CVE-2017-1000083

build() {
	abuild-meson \
		-Db_lto=true \
		-Dsystemduserunitdir=no \
		-Dgtk_doc=false \
		. output
	meson compile -C output
}

check() {
	meson test --no-rebuild --print-errorlogs -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

nautilus() {
	pkgdesc="$pkgname (Nautilus extension)"
	install_if="$pkgname=$pkgver-r$pkgrel nautilus"

	amove usr/lib/nautilus
}

libs() {
	default_libs
	mv "$pkgdir"/usr/lib/* "$subpkgdir"/usr/lib/
}

doc() {
	default_doc
	if [ -d "$pkgdir"/usr/share/help ]; then
		amove usr/share/help
	fi
}

sha512sums="
949f8bbfdb33d6d239d348a56ef95f1f8f5dacdd1b4099606c568aa648ef64b7a07732763baa49a75f585ed33b23e26440f9fde33dbc68467c98ae3ee2362ea5  evince-46.1.tar.xz
"
