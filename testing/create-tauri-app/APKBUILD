# Contributor: Matthias Ahouansou <matthias@ahouansou.cz>
# Maintainer: Matthias Ahouansou <matthias@ahouansou.cz>
pkgname=create-tauri-app
pkgver=3.14.0
pkgrel=0
pkgdesc="Build tool for Leptos"
url="https://tauri.app"
arch="all !s390x" # nix
license="MIT OR Apache-2.0"
makedepends="cargo-auditable"
depends="cargo"
subpackages="$pkgname-doc"
source="
	$pkgname-$pkgver.tar.gz::https://github.com/tauri-apps/create-tauri-app/archive/refs/tags/create-tauri-app-v$pkgver.tar.gz
"
options="net"
builddir="$srcdir/$pkgname-$pkgname-v$pkgver"

prepare() {
	default_prepare
	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --release --frozen
}

check() {
	cargo test --frozen
}

package() {
	install -Dm 755 target/release/cargo-create-tauri-app "$pkgdir"/usr/bin/cargo-create-tauri-app

	for l in _APACHE-2.0 _MIT .spdx
	do
		install -Dm 644 LICENSE"$l" "$pkgdir"/usr/share/licenses/"$pkgname"/LICENSE"$l"
	done
}

sha512sums="
d7afae650b09c5b79868d6f1b43977c9379af7aef1acddb33346090324f8da3afdc7a20909d969d1ccc50d2632c8bf07bfccd60a47c0c3aa0d03adf8052fb984  create-tauri-app-3.14.0.tar.gz
"
